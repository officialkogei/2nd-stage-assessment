<?php
    $colors = array('red','green','blue','white','yellow');

    //for($i = 0; $i< count($colors); $i++){
    //    echo $colors[$i]."\n";
    //}

    //I can use var_dump to display array and its Index
    //var_dump($colors);

    //I can use array_walk when i need to do something to each of the items in the array
    array_walk($colors, "custom_print");

    //function to print
    function custom_print($val, $key) {
        echo $val ."\n";;
    }
?>