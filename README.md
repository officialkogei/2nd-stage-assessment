# 2nd Stage Assessment

I am grateful if you have reached here;

Note:
Question Numbers are represented by folders Q1-Q5 respectively

For Q5. Develop a file parser service that consumes the log supplied and performs the following actions.

Prerequisites 
1) Php(Apache) Server (Xampp was used in the prototype) Recommended

2) SQL Server (Also Used SQLServer Management Studio for DB Management)

	Note: 	To use Php with MS SQL you will need to install Microsoft Drivers for PHP for SQL Server; This will depend on your php version;
		Use this link to download the Drivers 'https://www.microsoft.com/en-us/download/details.aspx?id=20098'
		Use this link for More Details 'https://www.youtube.com/watch?v=hxfdbnpOqSI'


3) Using SSMS run the SQL Scripts to setup the the Database (File found in the main directory cellulant_script.sql)
4) Create a folder in the puplic directory with the name 'Cellulant';
5) paste folders Q1, Q2, Q3, Q4, Q5 in the newly created folder 'Cellulant';
6)edit the connection string and put your connection parameters in file conn.php in the directory '..Cellulant/Q5';
7) Start apache/php server and using a browser enter the url Example 'http://localhost:86/Cellulant/Q5/conn.php'; remember to use your port number on the URL
	if you get a blank screen, you are good to proceed; otherwise check if you have ommited one step or the connection string is not correct;

8) Now Run/ navigate to 'http://localhost:86/Cellulant/Q5/save_logs_to_sqlserver.php';
	a)logs data from logs.txt will be persisted in Cellulant database on  Table Test2
	b) a new txt file called 'custom_logs will be created and populated with logs for a) above. Success and Errors including duplicate entires


I am very thankful to Cellulant