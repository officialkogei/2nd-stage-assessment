<?php
    header('Content-type: text/plain');
    include_once ('conn.php');

    $sql = "SELECT * FROM Test2";
    $data = sqlsrv_query($conn, $sql);

    $response = array();

    while($row = sqlsrv_fetch_object($data)){

        array_push( $response,array ("Id"=>$row->Id, "Requesttime"=>$row->Requesttime, "Type"=>$row->Type,
            "ResourceUrl"=>$row->ResourceUrl, "REQUEST_IP"=>"REQUEST_IP", "TAT"=>$row->TAT, "CreatedUtc"=>$row->CreatedUtc));

    }

    echo json_encode($response);

    sqlsrv_close( $conn);

?>
