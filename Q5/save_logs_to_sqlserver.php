<?php

    //Include all requred resouces
    header('Content-type: application/json');
    include_once ('conn.php');
    include_once ('functions.php');
    error_reporting(0);
    //Open the Log file and Read Contents line by line
    if ($fh = fopen('logs.txt', 'r')) {
       
           //Reading Contents line by line and exploading as it creates temporary holder variables
        while (!feof($fh)) {
            $line = fgets($fh);

            $line = explode(" - ", $line);
            $Date = $line[0];

            $line = explode(" ] ", $line[1]);
            $Type = explode("[ ", $line[0])[1];

            $line = explode(" | ", $line[1]);
            $ResourceUrl = explode(': ', $line[0])[0];

            $REQUEST_IP  = explode('REQUEST_IP', $line[1])[1];
            $TAT = floatval(explode('TAT: ', $line[2])[1]);

            //Sql Statement to persist the logs in sql table Test2 in Database Cellulant
            $sql = "INSERT INTO Test2 (Requesttime, Type, ResourceUrl, REQUEST_IP, TAT) VALUES ('$Date', '$Type', '$ResourceUrl', '$REQUEST_IP', '$TAT' )";

            //Geting the results of the tsql run
            if(sqlsrv_query($conn, $sql)){
                //Logging errors in a text file called custom_logs Using a function called appendToTextFileLogs
                appendToTextFileLogs($Date, $Type, $ResourceUrl, $REQUEST_IP, $TAT, true, "Success");
            }else {

                //Logging errors in a text file called custom_logs Using a function called appendToTextFileLogs
                if( ($errors = sqlsrv_errors() ) != null) {
                    foreach( $errors as $error ) {
                        // echo "SLSTATE: ".$error[ 'SQLSTATE']."<br />";
                        // echo "code: ".$error[ 'code']."<br />";
                        // echo "message: ".$error[ 'message']."<br />";

                        $Message = " Error Code : " . $error[ 'code'] . " >>> " .$error[ 'message'];
                        appendToTextFileLogs($Date, $Type, $ResourceUrl, $REQUEST_IP, $TAT, false, $Message);

                    }
                }
            }


        }

        //Closing the log File
        fclose($fh);

        //Closing the SQL Connection
        sqlsrv_close( $conn);

    }
?>