<?php
/**
 * Created by PhpStorm.
 * User: starkogi
 * Date: 2018-09-27
 * Time: 10:05 PM
 */

function appendToTextFileLogs($Date, $Type, $ResourceUrl, $REQUEST_IP, $TAT, $logged, $Response)
{
	//Open file and Write Log Contents. (Creates the file if not exist)
    if ($fh = fopen('custom_logs.txt', 'a')) {

        fwrite($fh, "Date : " . $Date . " | Type :" . $Type . " | ResourceUrl : " . $ResourceUrl . " | REQUEST_IP : "
            . $REQUEST_IP . " | TAT : " . $TAT . " | logged : " . $logged  . " | Log Response : " . $Response . "\r\n");
    }

    //Close File for others to access
    fclose($fh);

    echo "Logged Successfully { " . "Date : " . $Date . " | Type :" . $Type . " | ResourceUrl : " . $ResourceUrl . " | REQUEST_IP : "
            . $REQUEST_IP . " | TAT : " . $TAT . " | logged : " . $logged  . " | Log Response : " . $Response . " } \r\n\r\n";

}